const BASE_URL = "https://62db6ca9d1d97b9e0c4f33de.mockapi.io/";

function getDSSV() {
  batLoading();
  axios({
    url: `${BASE_URL}/sv`,
    method: "GET",
  })
    .then(function (res) {
      tatLoading();
      renderDSSV(res.data);
    })
    .catch(function (err) {
      tatLoading();
    });
}
getDSSV();


function xoaSinhVien(id) {
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      getDSSV();
    })
    .catch(function (err) {
    });
}



function suaSinhVien(id) {
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "GET",
  })
    .then(function (res) {
      showThongTinLenForm(res.data);
    })
    .catch(function (err) {
    });
}

function themSV() {
  let newSV = {
    id: document.getElementById("txtMaSV").value,
    name: document.getElementById("txtTenSV").value,
    email: document.getElementById("txtEmail").value,
    passwork: document.getElementById("txtPass").value,
    math: document.getElementById("txtDiemToan").value,
    physics: document.getElementById("txtDiemLy").value,
    chemistry: document.getElementById("txtDiemHoa").value,
    tinhDTB: function () {
      return (this.math * 1 + this.physics * 1 + this.chemistry * 1) / 3
    }
  };

  batLoading();
  axios({
    url: `${BASE_URL}/sv`,
    method: "POST",
    data: newSV,
  })
    .then(function (res) {
      tatLoading();
      getDSSV();
    })
    .catch(function (err) {
      tatLoading();
    });
    
}

function capnhatSV() {
  let capNhat = {
    id: document.getElementById("txtMaSV").value,
    name: document.getElementById("txtTenSV").value,
    email: document.getElementById("txtEmail").value,
    password: document.getElementById("txtPass").value,
    math: document.getElementById("txtDiemToan").value * 1,
    physics: document.getElementById("txtDiemLy").value * 1,
    chemistry: document.getElementById("txtDiemHoa").value * 1,

  };
  batLoading();
  axios({
    url: `${BASE_URL}/sv/${capNhat.id}`,
    method: "PUT",
    data: capNhat,
  })
    .then(function (res) {
      tatLoading();
      getDSSV();
    })
    .catch(function (err) {
      tatLoading();
    });
  document.getElementById("txtMaSV").disabled = false;
}
function reset() {
  document.getElementById("txtTenSV").value = "";
  document.getElementById("txtEmail").value = "";
  document.getElementById("txtPass").value = "";
  document.getElementById("txtDiemToan").value = "";
  document.getElementById("txtDiemLy").value = "";
  document.getElementById("txtDiemHoa").value = "";
  document.getElementById("txtMaSV").value = "";
}
