renderDSSV = function (dssv) {
  var contentHTML = "";
  dssv.forEach((sv) => {
    var contentTr = `
   <tr>
   <td>${sv.id}</td>
   <td>${sv.name}</td>
   <td>${sv.email}</td>
   <td>${(sv.math * 1 + sv.physics * 1 + sv.chemistry * 1) / 3}</td>
   <td>
   <button 
   onclick=xoaSinhVien('${sv.id}')
   class="btn btn-danger">Xoá</button>
   <button onclick=suaSinhVien('${sv.id}') class="btn btn-warning">Sửa</button>
   </td>
   </tr>`;
    contentHTML += contentTr;
  });
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
};

function batLoading() {
  document.getElementById("loading").style.display = "flex";
}
function tatLoading() {
  document.getElementById("loading").style.display = "none";
}

function showThongTinLenForm(sv) {
  document.getElementById("txtMaSV").value = `${sv.id}`;
  document.getElementById("txtTenSV").value = `${sv.name}`;
  document.getElementById("txtEmail").value = `${sv.email}`;
  document.getElementById("txtPass").value = `${sv.password}`;
  document.getElementById("txtDiemToan").value = `${sv.math}`;
  document.getElementById("txtDiemLy").value = `${sv.physics}`;
  document.getElementById("txtDiemHoa").value = `${sv.chemistry}`;
}

